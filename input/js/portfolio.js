// @codekit-prepend 'lib/handlebars-v1.3.0.js'

var Portfolio = (function(){
  var s; // will become alias to settings below
  return {
    settings: {
      template: document.getElementById('template'),
      behance: {
        api_key: 'EnR7QnhfSFEdsNRoDyvIHTHyG59wB29T',
        user_id: 'courtneymyers'
      },
      work: document.getElementById('work')
    },
    init: function(){
      s = this.settings;
      this.retreiveJSONP();
    },
    retreiveJSONP: function(){
      // behance user url uses JSONP with custom callback method, 'Portfolio.prepareData' (see below)
      var behance_user_url = 'http://www.behance.net/v2/users/' + s.behance.user_id + '?api_key=' + s.behance.api_key + '&callback=Portfolio.prepareData';
      // JSONP injection
      var jsonp_script = document.createElement('script');
      jsonp_script.src = behance_user_url;
      document.body.appendChild(jsonp_script);
    },
    prepareData: function(data){
      // if 'behanceUser' (therefore, data) exists in session storage, render template
      if(sessionStorage.getItem('behanceUser')) {
        this.renderTemplate();
      } else {
        // converts data to a string (so it can be stored in session storage), stores it, and renders template
        var string_data = JSON.stringify(data);
        sessionStorage.setItem('behanceUser', string_data);
        this.renderTemplate();
      }
    },
    renderTemplate: function(){
      // parses string in session storage into JSON data
      var json_data = JSON.parse(sessionStorage.getItem('behanceUser'));
      // Handlebars mess
      var authored_template = s.template.innerHTML;         // gets the authored template content
      var callback = Handlebars.compile(authored_template); // compiles the template into a callback function
      var compiled_template = callback(json_data);          // calls the callback function with the json data
      s.work.innerHTML = compiled_template;                 // inserts rendered template into page
    }
  }; // return
}()); // IIFE

document.addEventListener('DOMContentLoaded', function(){
  Portfolio.init();
});